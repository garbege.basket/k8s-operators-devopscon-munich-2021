Deep diving into building an Operators
======================================

A mininal Operator in Go with go-client
---------------------------------------

We begin with re-creating `grafzahl` using the pure go-client.

Basic Structure
^^^^^^^^^^^^^^^
To begin with a modern go project we create a layout for our CLI and
package which is imported. We use go module to manage the dependencies::

   $ mkdir grafazahl/{cmd/pkg}
   $  cd grafzahl
   
   $ go mod init github.com/oz123/grafzahl
   
In ``cmd/main.go``:

.. code:: golang

   package main
   
   import (
   	"fmt"
   
   	operator "github.com/oz123/grafzahl/pkg"
   )
   
   func main() {
   	fmt.Println("Hello, Modules!")
   
   	operator.PrintHello()
   }

And in ``pkg/operator.go``:

.. code:: golang

   package operator
   
   import (
   	"fmt"
   )
   
   func PrintHello() {
   	fmt.Println("Hello, Modules! This is Operator speaking!")
   }
   
   var (
   	VERSION = "0.0.0.dev"
   )
   
   package main
   
   import (
   	"fmt"
   
   	operator "github.com/oz123/grafzahl/pkg"
   )
   
   func main() {
   	fmt.Println("Hello, Modules!")
   
   	operator.PrintHello()
   }

We also add a ``Makefile`` to help us managing the project::

   $ cat < EOF >Makefile
   DATE    = $(shell date +%Y%m%d%H%M)
   VERSION = v$(DATE)
   GOOS    ?= $(shell go env | grep GOOS | cut -d'"' -f2)
   BINARY  := grafzahl
   
   LDFLAGS := -X github.com/oz123/grafzahl/pkg/example.VERSION=$(VERSION)
   GOFLAGS := -ldflags "$(LDFLAGS)"
   
   SRCDIRS  := cmd pkg
   PACKAGES := $(shell find $(SRCDIRS) -type d)
   GOFILES  := $(addsuffix /*.go,$(PACKAGES))
   GOFILES  := $(wildcard $(GOFILES))
   
   .PHONY: all clean
   
   all: bin/$(GOOS)/$(BINARY)
   
   bin/%/$(BINARY): $(GOFILES) Makefile
   	GOOS=$* GOARCH=amd64 go build $(GOFLAGS) -v -o $*/$(BINARY) cmd/main.go
   
Compile and run the program::

   $ make
   GOOS=linux GOARCH=amd64 go build -ldflags "-X github.com/oz123/grafzahl/pkg/example.VERSION=v202112011115" -v -o linux/grafzahl cmd/main.go
   github.com/oz123/grafzahl/pkg
   command-line-arguments

   $ ./linux/grafzahl
   Hello, Modules!
   Hello, Modules! This is Operator speaking!

Get a kubernetes client
^^^^^^^^^^^^^^^^^^^^^^^

Time to add some real login here! We begin by defining a few structures and methods
to help us get a kubernetes client and start Grafzahl.

In ``pkg/operator/operator.go``:

.. code:: go

   package operator
   
   import (
   	"log"
   	"sync"
   	"time"
   
   	"k8s.io/client-go/kubernetes"
   	"k8s.io/client-go/rest"
   	"k8s.io/client-go/tools/clientcmd"
   )
   
   var (
   	VERSION      = "0.0.0.dev"
   	resyncPeriod = 10 * time.Second
   )
   
   type Options struct {
   	KubeConfig string
   }
   
   type Grafzahl struct {
   	Options
   	clientset *kubernetes.Clientset
   }
   
   func New(options Options) *Grafzahl {
   	config := newClientConfig(options)
   
   	clientset, err := kubernetes.NewForConfig(config)
   	if err != nil {
   		log.Fatalf("Couldn't create Kubernetes client: %s", err)
   	}
   
   	gz := &Grafzahl{
   		Options:   options,
   		clientset: clientset,
   	}
   
   	return gz
   }
   
   func (example *Grafzahl) Run(stopCh <-chan struct{}, wg *sync.WaitGroup) {
   	log.Printf("Grafzahl %v is ready to count \n", VERSION)
   }
   
   func newClientConfig(options Options) *rest.Config {
   	rules := clientcmd.NewDefaultClientConfigLoadingRules()
   	overrides := &clientcmd.ConfigOverrides{}
   
   	if options.KubeConfig != "" {
   		rules.ExplicitPath = options.KubeConfig
   	}
   
   	config, err := clientcmd.NewNonInteractiveDeferredLoadingClientConfig(rules, overrides).ClientConfig()
   	if err != nil {
   		log.Fatalf("Couldn't get Kubernetes default config: %s", err)
   	}
   
   	return config
   }

And in ``cmd/main.go``:

.. code:: go
   
   package main

   import (
   	"flag"
   	"log"
   	"os"
   	"os/signal"
   	"sync"
   	"syscall"
   
   	operator "github.com/oz123/grafzahl/pkg/operator"
   	"github.com/spf13/pflag"
   )
   
   var (
   	options operator.Options
   )
   
   func init() {
   	pflag.StringVar(&options.KubeConfig, "kubeconfig", "", "Path to kubeconfig file with authorization and master location information.")
   }
   
   func main() {
   
   	// Set logging output to standard console out
   	log.SetOutput(os.Stdout)
   
   	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)
   	pflag.Parse()
   	sigs := make(chan os.Signal, 1)
   	stop := make(chan struct{})
   	signal.Notify(sigs, os.Interrupt, syscall.SIGTERM) // Push signals into channel
   
   	wg := &sync.WaitGroup{} // Goroutines can add themselves to this to be waited on
   
   	operator.New(options).Run(stop, wg)
   
   	<-sigs // wait for signals (this hangs until a signal arrives)
   	log.Printf("shutting down... %v", operator.VERSION)
   
   	close(stop) // tell goroutines to stop themselves
   	wg.Wait()   // wait for all to be stopped
   }


When we run the code now it block, since it is continuously polling the kubernetes API server::

   $ ./linux/grafzahl --kubeconfig ~/.kube/config
   2021/12/01 12:26:00 Grafzahl v202112011225 is ready to count
   ^C2021/12/01 11:26:12 shutting down...  v202112011158

Watching kubernetes resources
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In the next part, we teach the operator how to query for kubernetes resources.

We add the following to ``pkg/operator/operator.go``:

.. code:: diff

   //diff -bur 02/pkg/operator/operator.go 03/pkg/operator/operator.go
   @@ -22,6 +22,8 @@
    type Grafzahl struct {
    	Options
    	clientset *kubernetes.Clientset
   +	podwatcher        *PodWatcher
   +	deploymentwatcher *DeploymentWatcher
    }
   
    func New(options Options) *Grafzahl {
   @@ -35,13 +37,18 @@
    	gz := &Grafzahl{
    		Options:   options,
    		clientset: clientset,
   +		podwatcher:        newPodWatacher(clientset),
   +		deploymentwatcher: newDeploymentWatcher(clientset),
    	}
   
    	return gz
    }
   
   -func (example *Grafzahl) Run(stopCh <-chan struct{}, wg *sync.WaitGroup) {
   +func (gz *Grafzahl) Run(stopCh <-chan struct{}, wg *sync.WaitGroup) {
    	log.Printf("Grafzahl %v is ready to count \n", VERSION)
   +
   +	go gz.podwatcher.Run(stopCh, wg)
   +	go gz.deploymentwatcher.Run(stopCh, wg)
    }


And we add two new files in ``pkg/operator``. The first is ``podwatcher.go``:

.. code:: go

   package operator

   import (
   	"fmt"
   	"log"
   	"sync"
   	"time"
   
   	corev1 "k8s.io/api/core/v1"
   	kubeinformers "k8s.io/client-go/informers"
   	"k8s.io/client-go/kubernetes"
   	"k8s.io/client-go/tools/cache"
   )
   
   type PodWatcher struct {
   	podInformer cache.SharedIndexInformer
   }
   
   func newPodWatacher(client *kubernetes.Clientset) *PodWatcher {
   	pw := &PodWatcher{}
   	kubeInformerFactory := kubeinformers.NewSharedInformerFactory(client, time.Second*30)
   
   	podInformer := kubeInformerFactory.Core().V1().Pods().Informer()
   
   	podInformer.AddEventHandler(cache.ResourceEventHandlerFuncs{
   		AddFunc: func(obj interface{}) {
   			pod := obj.(*corev1.Pod)
   			fmt.Printf("pod added: %s/%s\n", pod.Namespace, pod.Name)
   		},
   		DeleteFunc: func(obj interface{}) {
   			fmt.Printf("pod deleted: %s \n", obj)
   		},
   		UpdateFunc: func(oldObj, newObj interface{}) {
   			fmt.Printf("pod updated: %s \n", newObj)
   		},
   	})
   
   	pw.podInformer = podInformer
   	return pw
   }
   
   func (p *PodWatcher) Run(stopCh <-chan struct{}, wg *sync.WaitGroup) {
   	defer wg.Done()
   	wg.Add(1)
   
   	log.Printf("Start watching pods")
   
   	go p.podInformer.Run(stopCh)
   
   	<-stopCh
   }

The second file is ``deploymentwatcher.go``:

.. code:: go

   package operator
   
   import (
   	"fmt"
   	"log"
   	"sync"
   	"time"
   
   	appsv1 "k8s.io/api/apps/v1"
   	kubeinformers "k8s.io/client-go/informers"
   	"k8s.io/client-go/kubernetes"
   	"k8s.io/client-go/tools/cache"
   )
   
   type DeploymentWatcher struct {
   	deploymentInformer cache.SharedIndexInformer
   }
   
   func newDeploymentWatcher(client *kubernetes.Clientset) *DeploymentWatcher {
   	dw := &DeploymentWatcher{}
   	kubeInformerFactory := kubeinformers.NewSharedInformerFactory(client, time.Second*30)
   
   	deploymentInformer := kubeInformerFactory.Apps().V1().Deployments().Informer()
   
   	deploymentInformer.AddEventHandler(cache.ResourceEventHandlerFuncs{
   		AddFunc: func(obj interface{}) {
   			deployment := obj.(*appsv1.Deployment)
   			fmt.Printf("deployment added: %s/%s\n", deployment.Namespace, deployment.Name)
   		},
   		DeleteFunc: func(obj interface{}) {
   			fmt.Printf("deployment deleted: %s \n", obj)
   		},
   		UpdateFunc: func(oldObj, newObj interface{}) {
   			fmt.Printf("deployment updated: %s \n", newObj)
   		},
   	})
   
   	dw.deploymentInformer = deploymentInformer
   	return dw
   }
   
   func (d *DeploymentWatcher) Run(stopCh <-chan struct{}, wg *sync.WaitGroup) {
   	defer wg.Done()
   	wg.Add(1)
   
   	log.Printf("Start watching deployments")
   
   	go d.deploymentInformer.Run(stopCh)
   
   	<-stopCh
   
   }


When we run the operator now, it lists all the pods and deployments::

   $ ./linux/grafzahl --kubeconfig ~/.kube/config
   2021/12/01 14:31:12 Grafzahl v202112011430 is ready to count
   2021/12/01 14:31:12 Start watching deployments
   2021/12/01 14:31:12 Start watching pods
   deployment added: grafzahl/grafzahl-operator
   deployment added: kube-system/coredns
   pod added: kube-system/coredns-78fcd69978-7594n
   pod added: kube-system/kube-proxy-p7x5m
   pod added: kube-system/kindnet-svkhm
   pod added: kube-system/kube-apiserver-devopscon
   pod added: kube-system/storage-provisioner
   pod added: kube-system/etcd-devopscon
   pod added: kube-system/kindnet-56m6l
   pod added: kube-system/kube-controller-manager-devopscon
   pod added: kube-system/kube-proxy-mjksn
   pod added: kube-system/kube-scheduler-devopscon
   ^C2021/12/01 14:31:14 shutting down... v202112011430

Adding the operator businness logic
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The heart of the operator is the structure ``cache.ResourceEventHandlerFuncs`` which has
three fields: ``AddFunc``, ``DeleteFunc`` and ``UpdateFunc``. To each of those field
we added an anonymous function which prints the object or the object fields ``Namespace``
and ``Name``. Note, that to access these fields we need to case the generic ``obj``
to the correct type.

For larger project with complexer logic, one can exract the anonymous functions,
and pass their name:

.. code:: diff

   --- ../03/pkg/operator/deploymentwatcher.go	2021-12-01 13:00:05.356950684 +0100
   +++ pkg/operator/deploymentwatcher.go	2021-12-01 14:50:57.380448001 +0100
   @@ -16,6 +16,11 @@
    	deploymentInformer cache.SharedIndexInformer
    }
   
   +func onAdddeployment(obj interface{}) {
   +	d := obj.(*appsv1.Deployment)
   +	fmt.Printf("deployment added: %s/%s\n", d.Namespace, d.Name)
   +}
   +
    func newDeploymentWatcher(client *kubernetes.Clientset) *DeploymentWatcher {
    	dw := &DeploymentWatcher{}
    	kubeInformerFactory := kubeinformers.NewSharedInformerFactory(client, time.Second*30)
   @@ -23,10 +28,7 @@
    	deploymentInformer := kubeInformerFactory.Apps().V1().Deployments().Informer()
   
    	deploymentInformer.AddEventHandler(cache.ResourceEventHandlerFuncs{
   -		AddFunc: func(obj interface{}) {
   -			deployment := obj.(*appsv1.Deployment)
   -			fmt.Printf("deployment added: %s/%s\n", deployment.Namespace, deployment.Name)
   -		},
   +		AddFunc: onAdddeployment,
    		DeleteFunc: func(obj interface{}) {
    			fmt.Printf("deployment deleted: %s \n", obj)
    		},
   
Working with ``CustomResourceDefinitions``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


mkdir -pv apis/grafzahl/v1beta1
