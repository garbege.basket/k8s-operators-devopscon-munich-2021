package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/spf13/pflag"

	"github.com/oz123/grafzahl"
	"github.com/oz123/grafzahl/internal"
)

var kubeConfigPath string

func init() {
	pflag.StringVarP(&kubeConfigPath, "kubeconfig", "c", "", "Path to kubeconfig file with authorization and master location information.")
}

func run() error {
	// Set logging output to standard console out
	log.SetOutput(os.Stdout)
	pflag.Parse()

	log.Printf("Starting Grafzahl Operator %s", internal.Version)

	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer cancel()

	operator, err := grafzahl.New(kubeConfigPath)
	if err != nil {
		return err
	}

	if err := operator.Run(ctx); err != nil {
		return err
	}

	log.Printf("shutting down... %s", internal.Version)
	return nil
}

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}
