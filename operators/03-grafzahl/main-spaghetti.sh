#!/bin/bash
###
# Grafzahl likes counting things, and when there are more things to counts the happier
# he is.
# This silly operator just counts Deployments and Pods in the cluster.
###

# exit on undefined variable
set -u

NUM_PODS=$(kubectl get numbers.grafzahl.io total -o jsonpath='{.spec.pods}')
NUM_DEPLOYMENTS=$(kubectl get numbers.grafzahl.io total -o jsonpath='{.spec.deployments}')


function check_pods(){

	# create an assosiative array with all pod names and namespaces
	declare -A NEWPODS=()
	while read -r a b; do
		NEWPODS["$a"]="$b"
	done < <(kubectl get pods -l grafzahl!=counted -A -o=jsonpath="{range .items[*]}{.metadata.name}{' '}{.metadata.namespace}{'\n'}{end}")

	if [[ ${#NEWPODS[@]} -gt 0 ]]; then
		echo "Found ${#NEWPODS[@]} new pods, pod number increased! 🧛 is 😄!"

		for key in "${!NEWPODS[@]}"; do
			kubectl patch pod "$key" -n ${NEWPODS["$key"]} --type merge -p '{"metadata": {"labels": {"grafzahl": "counted"}}}'
		done
		PODS=$(( ${#NEWPODS[@]} + NUM_PODS ))
		kubectl patch numbers.grafzahl.io total --type merge -p "{\"spec\": {\"pods\": ${PODS}}}"
		retrun ${PODS}
	fi

	PODS=$(kubectl get pods -A -o json | jq '.items | length')

	if [[  ${PODS} -lt $NUM_PODS ]]; then
		echo "Pods number is the smaller! 🧛 is 😔"
		kubectl patch numbers.grafzahl.io total --type merge -p "{\"spec\": {\"pods\": ${PODS}}}"
	elif [[ $PODS -eq $NUM_PODS ]]; then
		echo "Pods number is the same! 🧛 is 😐!"
	fi

	return $PODS
}

function check_deployments(){

	# create an assosiative array with all deployments names and namespaces
	declare -A NEWDEPLOYMENTS=()
	while read -r a b; do
		NEWDEPLOYMENTS["$a"]="$b"
	done < <(kubectl get deployment -l grafzahl!=counted -A -o=jsonpath="{range .items[*]}{.metadata.name}{' '}{.metadata.namespace}{'\n'}{end}")

	if [[ ${#NEWDEPLOYMENTS[@]} -gt 0 ]]; then
		echo "Found ${#NEWDEPLOYMENTS[@]} new deployments, deployments number increased! 🧛 is 😄!"

		for key in "${!NEWDEPLOYMENTS[@]}"; do
			kubectl patch deployment "$key" -n ${NEWDEPLOYMENTS["$key"]} --type merge -p '{"metadata": {"labels": {"grafzahl": "counted"}}}'
		done

		DEPLOYMENTS=$(( ${#NEWDEPLOYMENTS[@]} + NUM_DEPLOYMENTS))
		kubectl patch numbers.grafzahl.io total --type merge -p "{\"spec\": {\"deployments\": ${DEPLOYMENTS}}}"
		return ${DEPLOYMENTS}
	fi

	DEPLOYMENTS=$(kubectl get deployments -A -o json | jq '.items | length')

	if [[  ${DEPLOYMENTS} -lt $NUM_DEPLOYMENTS ]]; then
		echo "Deployment number is the smaller! 🧛 is 😔"
		kubectl patch numbers.grafzahl.io total --type merge -p "{\"spec\": {\"deployments\": ${PODS}}}"
	elif [[ $DEPLOYMENTS -eq $NUM_DEPLOYMENTS ]]; then
		echo "Deployment number is the same! 🧛 is 😐!"
	fi

	return $DEPLOYMENTS
}


function check() {
	local resource=$1

	case $resource in
	
	deployment)
		PREVIOUS_NUM=$NUM_DEPLOYMENTS
		;;
	pod)
		PREVIOUS_NUM=$NUM_PODS
		;;
	*)
    		echo -n "Unknown resource";
		exit 1;
		;;

	esac
	# create an assosiative array with all deployments names and namespaces
	declare -A NEW=()
	while read -r a b; do
		NEW["$a"]="$b"
	done < <(kubectl get $resource -l grafzahl!=counted -A -o=jsonpath="{range .items[*]}{.metadata.name}{' '}{.metadata.namespace}{'\n'}{end}")

	if [[ ${#NEW[@]} -gt 0 ]]; then
		echo "Found ${#NEW[@]} new ${resource^}s, ${resource} number increased! 🧛 is 😄!"

		for key in "${!NEW[@]}"; do
			kubectl patch ${resource} "$key" -n ${NEW["$key"]} --type merge -p '{"metadata": {"labels": {"grafzahl": "counted"}}}'
		done

		NEWTOTAL=$(( ${#NEW[@]} + PREVIOUS_NUM ))

		return $NEWTOTAL
	fi

	ALL=$(kubectl get ${resource} -A -o json | jq '.items | length')


	if [[  ${ALL} -lt ${PREVIOUS_NUM} ]]; then
		echo "${resource^} number is the smaller! 🧛 is 😔"
		kubectl patch numbers.grafzahl.io total --type merge -p "{\"spec\": {\"${resource}s\": ${ALL}}}"
	elif [[ ${ALL} -eq ${PREVIOUS_NUM} ]]; then
		echo "${resource^} number is the same! 🧛 is 😐!"
	fi

	return $ALL
}

while true
do
	check pod
	NUM_PODS=$?
	check deployment
	NUM_DEPLOYMENTS=$?
	sleep 5
done
