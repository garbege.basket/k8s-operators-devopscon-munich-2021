#!/bin/bash
###
# Grafzahl likes counting things, and when there are more things to counts the happier
# he is.
# This silly operator just counts Deployments and Pods in the cluster.
###

# tell bash to crash if any error is encountered
set -e 

NUM_PODS=$(kubectl get numbers.grafzahl.io total -o jsonpath='{.spec.pods}')
NUM_DEPLOYMENTS=$(kubectl get numbers.grafzahl.io total -o jsonpath='{.spec.deployments}')

while true
do
	DEPLOYMENTS=$(kubectl get deployments.apps -A -o json | jq '.items | length')
	PODS=$(kubectl get pods -A -o json | jq '.items | length')
	PATCH_P=true
	PATCH_D=true
	echo "Deployments now ${DEPLOYMENTS}, previously ${NUM_DEPLOYMENTS}"
	echo "Pods now ${PODS}, previously  ${NUM_PODS}"
	if [[ $PODS -gt $NUM_PODS ]]; then
		echo "Pods number increased! 🧛 is 😄!"
	elif [[  $PODS -lt $NUM_PODS ]]; then
		echo "Pods number is the smaller! 🧛 is 😔"
	elif [[ $PODS -eq $NUM_PODS ]]; then
		echo "Pods number is the same! 🧛 is 😐!"
		PATCH_P=false
	fi

	if [[ $DEPLOYMENTS -gt $NUM_DEPLOYMENTS ]]; then
		echo "Deployments number increased! 🧛 is 😄!"
	elif [[  $DEPLOYMENTS -lt $NUM_DEPLOYMENTS ]]; then
		echo "Deployments number is the smaller! 🧛 is !😔"
	elif [[ $DEPLOYMENTS -eq $NUM_DEPLOYMENTS ]]; then
		echo "Deployments number is the same! 🧛 is 😐!"
		PATCH_D=false
	fi
	
	if [ ${PATCH_P} == true ]; then
		kubectl patch numbers.grafzahl.io total --type merge -p "{\"spec\": {\"pods\": ${PODS}}}"
	fi
	
	if [ ${PATCH_D} == true ]; then
		kubectl patch numbers.grafzahl.io total --type merge -p "{\"spec\": {\"pods\": ${DEPLOYMENTS}}}"
	fi
	
	NUM_PODS=${PODS}
	NUM_DEPLOYMENTS=${DEPLOYMENTS}

	sleep 10
done
